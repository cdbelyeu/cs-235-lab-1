#pragma once
#include "Fighter.h"
#include "FighterInterface.h"

class Cleric : public Fighter
{
	public:
		Cleric();
		~Cleric();
		Cleric(string name, int maxHp, int strength, int speed, int magic);

		int currentMana;
		int maxMana;



		void reset();
		void regenerate();
		bool useAbility();


		/*
		*	getDamage()
		*
		*	Returns the amount of damage a fighter will deal.
		*
		*	Robot:
		*	This value is equal to the Robot's strength plus any additional damage added for having just used its special ability.
		*
		*	Archer:
		*	This value is equal to the Archer's speed.
		*
		*	Cleric:
		*	This value is equal to the Cleric's magic.
		*/
		virtual int getDamage();

};
