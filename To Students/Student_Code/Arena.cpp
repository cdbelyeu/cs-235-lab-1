#include "Arena.h"
#include <vector>
#include <sstream>
#include <string.h>

#include "Cleric.h"
#include "Robot.h"
#include "Archer.h"

using namespace std;


Arena::Arena()
{
	this->fighters = vector<FighterInterface*>();
}


Arena::~Arena()
{
}

/*
*addFighter(string)
*
*	Adds a new fighter to the collection of fighters in the arena.Do not allow
*	duplicate names.Reject any string that does not adhere to the format
*	outlined in the lab specs.
*
*	Return true if a new fighter was added; false otherwise.
*/

bool Arena::addFighter(string info)
{
	stringstream buffer;
	buffer.str(info);
	//stringstream buffer = stringstream(info);

	string name;
	string fighterClass;

	int hp;
	int strength;
	int speed;
	int magic;

	//validation
	int numberOfWords = 1;
	for (int i = 0; i < info.length(); i++)
		if (info[i] == ' ')
			numberOfWords++;

	if (numberOfWords != 6 || info[(info.length()) - 1] == ' ')
		return false;


	//this will currently destabilize if you pass in strings
	//for integer values
	buffer >> name;
	buffer >> fighterClass;
	buffer >> hp;
	buffer >> strength;
	buffer >> speed;
	buffer >> magic;

	for (int i = 0; i < this->fighters.size(); i++)
		if (this->fighters[i]->getName() == name)
			return false;

	if (hp < 1 || strength < 1 || speed < 1 || magic < 1)
		return false;

	//Robot(string name, int maxHp, int strength, int speed, int magic)
	if (fighterClass == "C")
		this->fighters.push_back(new Cleric(name, hp, strength, speed, magic));
	else if (fighterClass == "A")
		this->fighters.push_back(new Archer(name, hp, strength, speed, magic));
	else if (fighterClass == "R")
		this->fighters.push_back(new Robot(name, hp, strength, speed, magic));
	else
		return false;
	return true;
	
}

/*
*	removeFighter(string)
*
*	Removes the fighter whose name is equal to the given name.  Does nothing if
*	no fighter is found with the given name.
*
*	Return true if a fighter is removed; false otherwise.
*/
bool Arena::removeFighter(string name)
{
	for (int i = 0; i < this->fighters.size(); i++)
		if(this->fighters[i]->getName() == name)
		{
			this->fighters[i] = this->fighters[this->fighters.size() - 1];
			this->fighters.pop_back();
			return true;
		}
	return false;
}

/*
*	getFighter(string)
*
*	Returns the memory address of a fighter whose name is equal to the given
*	name.  Returns NULL if no fighter is found with the given name.
*
*	Return a memory address if a fighter is found; NULL otherwise.
*/
FighterInterface* Arena::getFighter(string name)
{
	for (int i = 0; i < this->fighters.size(); i++)
		if (this->fighters[i]->getName() == name)
			return this->fighters[i];
	return NULL;
}

/*
*	getSize()
*
*	Returns the number of fighters in the arena.
*
*	Return a non-negative integer.
*/
int Arena::getSize()
{
	return this->fighters.size();
}