#include "Cleric.h"
#include "Fighter.h"

using namespace std;

Cleric::Cleric()
{

}

Cleric::~Cleric()
{

}

Cleric::Cleric(string name, int maxHp, int strength, int speed, int magic) :Fighter(name, maxHp, strength, speed, magic)
{
	this->currentMana = magic * 5;
	this->maxMana = magic * 5;
}

void Cleric::reset()
{
	this->currentMana = this->maxMana;
	this->Fighter::reset();
}

void Cleric::regenerate()
{
	int factor = this->magic / 5;
	if (factor < 1)
		factor = 1;

	this->currentMana += factor;

	if (this->currentMana > this->maxMana)
		this->currentMana = this->maxMana;

	this->Fighter::regenerate();
}


bool Cleric::useAbility()
{
	if(this->currentMana < CLERIC_ABILITY_COST)
		return false;

	int factor = this->magic / 3;
	this->currentMana -= CLERIC_ABILITY_COST;

	if (factor < 1)
		factor = 1;

	this->currentHp += factor;

	if (this->currentHp > this->maxHp)
		this->currentHp = this->maxHp;

	return true;
}

int Cleric::getDamage()
{
	return this->magic;
}