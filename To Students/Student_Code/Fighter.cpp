#include "Fighter.h"

using namespace std;

Fighter::Fighter()
{

}

Fighter::~Fighter()
{

}

Fighter::Fighter(string name, int maxHp, int strength, int speed, int magic)
{
	this->name = name;
	this->maxHp = maxHp;
	this->strength = strength;
	this->speed = speed;
	this->magic = magic;
}

string Fighter::getName()
{
	return this->name;
}

int Fighter::getCurrentHP()
{
	return this->currentHp;
}

int Fighter::getMaximumHP()
{
	return this->maxHp;
}

int Fighter::getStrength()
{
	return this->strength;
}

int Fighter::getSpeed()
{
	return this->speed;
}

int Fighter::getMagic()
{
	return this->magic;
}

int Fighter::getDamage()
{
	return this->damage;
}


void Fighter::takeDamage(int damage)
{
	damage = damage - this->speed / 4;
	
	if (damage < 1)
		damage = 1;

	this->currentHp -= damage;
}

//this will be extended within the child classes
void Fighter::reset()
{
	this->currentHp = this->maxHp;
}

/*
*	regenerate()
*
*	Increases the fighter's current hit points by an amount equal to one sixth of
*	the fighter's strength.  This method must increase the fighter's current hit
*	points by at least one.  Do not allow the current hit points to exceed the
*	maximum hit points.
*
*	Cleric:
*	Also increases a Cleric's current mana by an amount equal to one fifth of the
*	Cleric's magic.  This method must increase the Cleric's current mana by at
*	least one.  Do not allow the current mana to exceed the maximum mana.
*/

void Fighter::regenerate()
{
	int factor = this->strength / 6;
	if (factor < 1)
		factor = 1;

	this->currentHp += factor;
	if (this->currentHp > this->maxHp)
		this->currentHp = this->maxHp;
}

bool Fighter::useAbility()
{
	return false;
}