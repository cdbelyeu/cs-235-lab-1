#pragma once
#include "Fighter.h"

class Archer : public Fighter
{
public:
	Archer();
	Archer(string name, int maxHp, int strength, int speed, int magic);
	~Archer();

	int baseSpeed;

	void reset();
	bool useAbility();
	int getDamage();

};
