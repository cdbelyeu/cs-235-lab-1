#include "Robot.h"

using namespace std;

Robot::Robot()
{

}

Robot::~Robot()
{

}

Robot::Robot(string name, int maxHp, int strength, int speed, int magic) :Fighter(name, maxHp, strength, speed, magic)
{
	this->maxEnergy = magic * 2;
	this->currentEnergy = maxEnergy;
	this->bonusDamage = 0;
}

void Robot::reset()
{
	this->currentEnergy = this->magic * 2;
	this->bonusDamage = 0;
	this->Fighter::reset();
}

bool Robot::useAbility()
{
	if (this->currentEnergy < ROBOT_ABILITY_COST)
		return false;

	double ratio = this->currentEnergy * 1.0 / this->maxEnergy;
	this->bonusDamage = strength * ratio * ratio * ratio * ratio;

	this->currentEnergy -= ROBOT_ABILITY_COST;
	return true;
}

int Robot::getDamage()
{
	int factor = this->bonusDamage;
	this->bonusDamage = 0;
	return this->strength + factor;
}