#include "Archer.h"

using namespace std;

Archer::Archer()
{

}

Archer::~Archer()
{

}

Archer::Archer(string name, int maxHp, int strength, int speed, int magic):Fighter(name, maxHp, strength, speed, magic)
{
	this->baseSpeed = speed;
}

void Archer::reset()
{
	this->speed = this->baseSpeed;
	Fighter::reset();
}

bool Archer::useAbility()
{
	this->speed++;
	return true;
}

int Archer::getDamage()
{
	return this->speed;
}